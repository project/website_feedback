INTRODUCTION
============

The module gives the website owners a mechanism to collect feedback from site
users along with a screenshot of the page the user is submitting the feedback
from. Users who have the permission to submit feedback will see a floating
feedback button which will open a popup form into which they can submit the
feedback comments and also capture the screenshot of the current page.

FEATURES
--------

The form allows the user to submit the following details

- Summary
- Description
- Screenshot of the page
- Additional image upload
- URL from where the feedback is submitted
- Type of feedback - feedback/support/bug report

The submission is created as a new entity which is made available in an
admin listing page. There is a basic workflow feature that allows site
administrator to mark the item as resolved.

Website owners can integrate the feedback submission into their own
internal workflows or external systems by plugging into the entity workflows
or by making the entity data available through feeds.

REQUIREMENTS
------------

Website Feedback module requires html2canvas library.
By default, it uses the library from CDN. But you can install it locally.

INSTALLATION
------------

You can install the module like any other Drupal module using composer or drush
or manually downloading it into your modules folder.

To use local Html2canvas library:

1. Download it from https://github.com/niklasvh/html2canvas.
2. Place it into libraries folder, so that there's a
   libraries/html2canvas/html2canvas.min.js file, in addition to the other
   files included in the library.
3. In the module's configuration page uncheck
   'Load Html2canvas library from CDN' option

CONFIGURATION
-------------
1. Visit the configuration page at:
   'Administration >> Configuration >> Development >> Website feedback'
2. You can change the following items:
   - Enable / disable the feedback type selector.
   - Enable tagging feedback from a vocabulary.
   - Change how the screenshot is taken - either using html2canvas or
     getDisplayMedia
   - Change the feedback button text.
   - Chnage the success message shown to user on success.
   - Use local file or CDN for the html2canvas library.
